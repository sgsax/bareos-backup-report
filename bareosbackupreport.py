#!/usr/bin/python3

# bareosbackupreport.py
#
# Seth D. Galitzer <sgsax@ksu.edu>
# April 13, 2018
#
# Generates a nicely-printed report of backup jobs. By default, reports all jobs
#  completed in last 24 hours. Parameters can be provided by the user to
#  override the timeframe and a partial jobname to filter results.
#
# Based on a script for reporting Bacula jobs found here:
#  http://www.revpol.com/baculasummaryemails

import argparse
import psycopg2

dbuser = "bareos"       # your Bareos database user
db = "bareos"           # your Bareos database name
dbpass = "password"     # your Bareos database password

hours = 24
filt = ""
sqlfilt = "%"
tag = "jobs"

def get_type(t):
    if t is "B":
        ret = "Backup"
    elif t is "R":
        ret = "Restore"

    return ret

def get_level(level):
    if level is "F":
        ret = "Full"
    elif level is "I":
        ret = "Incremental"
    elif level is "D":
        ret = "Differential"

    return ret

def get_status(status):
    if status is "A":
        ret = "Canceled by user"
    elif status is "p":
        ret = "Waiting for higher priority jobs to finish"
    elif status is "R":
        ret = "Running"
    elif status is "T":
        ret = "Terminated normally"
    elif status is "W":
        ret = "Terminated with warnings"

    return ret
        
parser = argparse.ArgumentParser(description="Generate basic bareos report")
parser.add_argument("-t", "--time", help="time in hours to gather report from (default 24)", action="store", dest="hours", type=int)
parser.add_argument("-f", "--filter", help="filter on partial job name to limit output", action="store", dest="filt")
args = parser.parse_args()

if args.hours:
    hours = args.hours

if args.filt:
    filt = args.filt

if filt is not "":
    sqlfilt = "%" + filt + "%"

sql = "SELECT JobId, Name, StartTime, EndTime, Type, Level, JobStatus, JobFiles, JobBytes FROM Job WHERE (RealEndTime >= NOW() - '%s hour'::INTERVAL) AND (Name like %s) ORDER BY JobID;"
sqlvars = (hours, sqlfilt, )

conn = psycopg2.connect("dbname='"+db+"' user='"+dbuser+"' password='"+dbpass+"' host='localhost'")
cursor = conn.cursor()
cursor.execute(sql, sqlvars)
data = cursor.fetchall()
cursor.close()
conn.close()

count=len(data)
if count == 1:
    tag = "job"

print("Nightly backup report ({0} {1})".format(len(data), tag))
print("")

for i in range(0, len(data)):
    print("Job ID       : " + str(data[i][0]))
    print("Job Name     : " + data[i][1])
    print("Start Time   : " + str(data[i][2]))
    print("End Time     : " + str(data[i][3]))
    print("Job Type     : " + get_type(data[i][4]))
    print("Backup Level : " + get_level(data[i][5]))
    print("Job Status   : " + get_status(data[i][6]))
    print("File Count   : " + str(data[i][7]))
    print("Job Bytes    : {0:.2f} MB".format(data[i][8]/(1024*1024)))
    print("")


