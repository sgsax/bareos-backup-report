# bareosbackupreport.py

Generates a nicely-printed report of backup jobs.

By default, reports all jobs completed in last 24 hours.

Parameters can be provided by the user to override the timeframe and a partial jobname to filter results.

Based on a shell script for reporting Bacula jobs found here: http://www.revpol.com/baculasummaryemails

Requires:
* Python3
* psycopg2 (Python PostgreSQL library)

